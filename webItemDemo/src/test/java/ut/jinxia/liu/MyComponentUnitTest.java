package ut.jinxia.liu;

import org.junit.Test;
import jinxia.liu.api.MyPluginComponent;
import jinxia.liu.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}